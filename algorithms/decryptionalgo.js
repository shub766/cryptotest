const express = require('express')
const router = express.Router()
const crypto = require('crypto');
const NodeRSA = require('node-rsa');
const path = require("path");
const fs = require("fs");


function getprivatekey(relativeOrAbsolutePathtoPrivateKey){
    var absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey);
    var privateKey = fs.readFileSync(absolutePath, "utf8");
    return privateKey;

}

var decryptStringWithRsaPrivateKey = function(toDecrypt, relativeOrAbsolutePathtoPrivateKey) {
    var privateKey = getprivatekey(relativeOrAbsolutePathtoPrivateKey)
    var buffer = Buffer.from(toDecrypt, "base64");
    var decrypted = crypto.privateDecrypt(privateKey, buffer);
    return decrypted.toString("hex");
};

function shadecryption(encdata,rsadecryption){
    let hash = crypto
        .createHash('sha512')
        .update(encdata+rsadecryption)
        .digest('hex');
    return hash;
}

function aesdecryption(rsadecryption,encdata){
    let iv = crypto.randomBytes(16);
    let keyone = rsadecryption;
    let decipher = crypto.createDecipheriv('aes-256-cbc',keyone,iv);
    console.log(decipher);
    decipher.setAutoPadding(false);
    let decrypted = decipher.update(encdata,'hex','base64');
    console.log(decrypted);
    decrypted += decipher.final('base64');
    console.log('decrypted',decrypted)
}

module.exports = {
    decryptStringWithRsaPrivateKey,getprivatekey,decryptStringWithRsaPrivateKey,shadecryption,aesdecryption
}